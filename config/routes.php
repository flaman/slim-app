<?php

// Define app routes

use Slim\App;

return function (App $app) {

    $app->get('/', \App\Action\HomeAction::class);

    $app->post('/login', \App\Action\LoginAction::class);

    $app->post('/users', \App\Action\User\UserCreateAction::class);

    $app->put('/users/{email}', \App\Action\User\UserUpdateAction::class);

    $app->get('/users/{email}', \App\Action\User\UserFindByEmailAction::class);

    $app->get('/users', \App\Action\User\UsersAction::class);
};

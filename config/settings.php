<?php

// Should be set to 0 in production
error_reporting(E_ALL);

// Should be set to '0' in production
ini_set('display_errors', '1');

// Settings
$settings = [];

// Path settings
$settings['root']   = dirname(__DIR__);
$settings['temp']   = $settings['root'] . '/tmp';
$settings['public'] = $settings['root'] . '/public';

// Database settings
$settings['db'] = [
    'driver'           => \Cake\Database\Driver\Mysql::class,
    'host'             => '192.168.56.102',
    'database'         => 'blog',
    'username'         => 'guest',
    'password'         => '123456',
    'charset'          => 'utf8mb4',
    'encoding'         => 'utf8mb4',
    'collation'        => 'utf8mb4_unicode_ci',
    // Enable identifier quoting
    'quoteIdentifiers' => true,
    // Set to null to use MySQL servers timezone
    'timezone'         => null,
    // Disable meta data cache
    'cacheMetadata'    => false,
    // Disable query logging
    'log'              => false,
    // PDO options
    'flags'            => [
        // Turn off persistent connections
        PDO::ATTR_PERSISTENT         => false,
        // Enable exceptions
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        // Emulate prepared statements
        PDO::ATTR_EMULATE_PREPARES   => true,
        // Set default fetch mode to array
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        // Convert numeric values to strings when fetching.
        // Since PHP 8.1 integers and floats in result sets will be returned using native PHP types.
        // This option restores the previous behavior.
        PDO::ATTR_STRINGIFY_FETCHES  => true,
    ],
];

if (getenv("DOCKER")) {
    $settings['db']['host']     = getenv('MYSQL_HOST');
    $settings['db']['username'] = getenv('MYSQL_USER');
    $settings['db']['password'] = getenv('MYSQL_PASSWORD');
}

// Phoenix settings
$settings['phoenix'] = [
    'migration_dirs'      => [
        'first' => __DIR__ . '/../resources/migrations',
    ],
    'environments'        => [
        'local' => [
            'adapter'  => 'mysql',
            'host'     => $settings['db']['host'],
            'port'     => 3306,
            'username' => $settings['db']['username'],
            'password' => $settings['db']['password'],
            'db_name'  => $settings['db']['database'],
            'charset'  => 'utf8',
        ],

    ],
    'default_environment' => 'local',
];

// Console commands
$settings['commands'] = [
    'app:init'     => \App\Console\InitializationsCommand::class,
    'app:route_list'  => \App\Console\RouteListCommand::class,
    'user:create' => \App\Console\UserCreateCommand::class,
];

$settings['JWT_SECRET'] = 'supersecretkeyyoushouldnotcommittogithub';

return $settings;

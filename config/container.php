<?php

use Cake\Database\Connection;
use Nyholm\Psr7\Factory\Psr17Factory;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Selective\BasePath\BasePathMiddleware;
use Slim\App;
use Slim\Factory\AppFactory;

return [
    'settings'                      => function () {
        return require __DIR__ . '/settings.php';
    },

    App::class                      => function (ContainerInterface $container) {
        AppFactory::setContainer($container);

        return AppFactory::create();
    },

    ResponseFactoryInterface::class => function (ContainerInterface $container) {
        return $container->get(Psr17Factory::class);
    },

    BasePathMiddleware::class       => function (ContainerInterface $container) {
        return new BasePathMiddleware($container->get(App::class));
    },

    // Database connection
    Connection::class               => function (ContainerInterface $container) {
        return new Connection($container->get('settings')['db']);
    },

    PDO::class                      => function (ContainerInterface $container) {
        $db     = $container->get(Connection::class);
        $driver = $db->getDriver();
        $driver->connect();

        return $driver->getConnection();
    },

    // RouteCollectorInterface::class  => function (ContainerInterface $container) {
    //     // Register routes
    //     (require __DIR__ . '/routes.php')($container->get(App::class));
    //     return $container->get(App::class)->getRouteCollector();
    // },

];

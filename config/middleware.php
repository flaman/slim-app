<?php

use App\Middleware\HttpHeadersMiddleware;
use Selective\BasePath\BasePathMiddleware;
use Slim\App;
use Tuupola\Middleware\CorsMiddleware;
use Tuupola\Middleware\JwtAuthentication;

return function (App $app) {
    // Parse json, form data and xml
    $app->addBodyParsingMiddleware();

    // Add the Slim built-in routing middleware
    $app->addRoutingMiddleware();

    $app->add(BasePathMiddleware::class);

    // Catch exceptions and errors
    $app->addErrorMiddleware(true, true, true);

    $app->add(new CorsMiddleware([
        "origin"         => ["*"],
        "methods"        => ["GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"],
        "headers.allow"  => ['Content-Type', 'Accept', 'Origin', 'Authorization'],
        "headers.expose" => [],
        "credentials"    => false,
        "cache"          => 0,
        "error"          => function ($response, $arguments) {
            $response->getBody()->write(
                json_encode($arguments, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT)
            );
            return $response->withHeader("Content-Type", "application/json");
        },
    ]));

    $app->add(new JwtAuthentication([
        "path"      => "/",
        "ignore"    => ["/login"],
        "secret"    => $app->getContainer()->get('settings')['JWT_SECRET'],
        "attribute" => false,
        "relaxed"   => ["192.168.50.52", "127.0.0.1", "localhost"],
        "error"     => function ($response, $arguments) {
            $response->getBody()->write(
                json_encode($arguments, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT)
            );
            return $response->withHeader("Content-Type", "application/json");
        },
    ]));

    $app->add(HttpHeadersMiddleware::class);

};

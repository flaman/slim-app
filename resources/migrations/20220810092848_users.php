<?php

declare (strict_types = 1);

use Phoenix\Migration\AbstractMigration;

final class Users extends AbstractMigration
{
    protected function up(): void
    {
        $this->table('users', 'id')
            ->setCharset('utf8mb4')
            ->setCollation('utf8mb4_unicode_ci')
            ->addColumn('id', 'integer', ['autoincrement' => true])
            ->addColumn('username', 'string', ['null' => true])
            ->addColumn('password', 'string', ['null' => true])
            ->addColumn('email', 'string', ['null' => true])
            ->addColumn('given_name', 'string', ['null' => true])
            ->addColumn('family_name', 'string', ['null' => true])
            ->addColumn('date_of_birth', 'date', ['null' => true])
            ->addColumn('enabled', 'boolean', ['default' => false])
            ->addColumn('created_at', 'datetime', ['null' => true, 'default' => 'CURRENT_TIMESTAMP'])
            ->addIndex('username', 'unique', 'btree', 'username')
            ->create();

    }

    protected function down(): void
    {
        $this->table('users')
            ->drop();
    }
}

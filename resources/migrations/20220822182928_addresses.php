<?php

declare (strict_types = 1);

use Phoenix\Migration\AbstractMigration;

final class Addresses extends AbstractMigration
{
    protected function up(): void
    {
        $this->table('addresses', 'id')
            ->setCharset('utf8mb4')
            ->setCollation('utf8mb4_unicode_ci')
            ->addColumn('id', 'integer', ['autoincrement' => true])
            ->addColumn('user_id', 'integer')
            ->addColumn('street', 'string', ['null' => true])
            ->addColumn('city', 'string', ['null' => true])
            ->addColumn('postal_code', 'string', ['null' => true])
            ->addColumn('country_code', 'string', ['null' => true, 'length' => 2])
            ->addColumn('lat', 'double', ['null' => true, 'decimals' => 8])
            ->addColumn('lng', 'double', ['null' => true, 'decimals' => 8])
            ->addIndex('user_id', '', 'btree', 'FK_addresses_users')
            ->addForeignKey('user_id', 'users', 'id', 'cascade', 'cascade')
            ->create();

    }

    protected function down(): void
    {
        $this->table('addresses')
            ->drop();
    }
}

<?php

namespace App\Support;

class Password
{
    const NUMBER     = 1;
    const LOWER_CASE = 2;
    const UPPER_CASE = 4;
    const SYMBOL     = 8;

    public static function make(int $len = 8, int $flag = 1): ?string
    {
        if ($len <= 0) {
            return null;
        }

        $all = '';

        $all_nb = '0123456789';
        $all_lw = 'abcdefghijklmnopqrstuvwxyz';
        $all_up = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $all_sy = '-.:;_$%&()=';

        $lw = '';
        $up = '';
        $nb = '';
        $sy = '';

        $n = 0;

        if ($flag&1) {
            $nb = str_shuffle($all_nb)[0];
            $all .= $all_nb;
            ++$n;
        }

        if ($flag&2) {
            $lw = str_shuffle($all_lw)[0];
            $all .= $all_lw;
            ++$n;
        }

        if ($flag&4) {
            $up = str_shuffle($all_up)[0];
            $all .= $all_up;
            ++$n;
        }

        if ($flag&8) {
            for ($i = 0; $i < 2; $i++) {
                $sy .= str_shuffle($all_sy)[0];
                $all .= $all_sy;
                ++$n;
            }
        }

        if (strlen($all) < $len) {
            $all = str_repeat($all, ceil($len / strlen($all)));
        }

        $passwd = $lw . $up . $nb . $sy . substr(str_shuffle($all), 0, $len - $n);

        return str_shuffle($passwd);
    }

    public static function validate(string $passwd, int $len, int $flag): bool
    {
        $number     = $flag&self::NUMBER ? '(?=.*[0-9])' : '';
        $lower_case = $flag&self::LOWER_CASE ? '(?=.*[a-z])' : '';
        $upper_case = $flag&self::UPPER_CASE ? '(?=.*[A-Z])' : '';
        $symbol     = $flag&self::SYMBOL ? '(?=.*[-.:;_$%&()=].*[-.:;_$%&()=])' : '';

        $regex = "/^{$number}{$lower_case}{$upper_case}{$symbol}.{{$len},20}$/";

        if (!preg_match($regex, $passwd)) {
            return false;
        }

        for ($i = 0; $i < strlen($passwd) - 1; $i++) {
            if ($passwd[$i] == $passwd[$i + 1]) {
                return false;
            }
        }

        return true;
    }
}

<?php

namespace App\Support;

use DateTime;
use Firebase\JWT\JWT;

class Token
{
    private $jwt_secret = '';

    public function setJwtSecret(string $jwt_secret)
    {
        $this->jwt_secret = $jwt_secret;
    }

    public function get(array $user)
    {

        $now    = new DateTime();
        $future = new DateTime("now +1 hours");

        $payload = [
            'name'        => $user['given_name'] . ' ' . $user['family_name'],
            'email'       => $user['email'],
            'given_name'  => $user['given_name'],
            'family_name' => $user['family_name'],
            "iat"         => $now->getTimeStamp(),
            "exp"         => $future->getTimeStamp(),
        ];

        $token  = JWT::encode($payload, $this->jwt_secret, "HS256");

        $data["jwt"] = $token;
        // $data["expires"] = $future->getTimeStamp();

        return $data;
    }
}

<?php

namespace App\Support;

class Strings
{
    /**
     * trasforma la stringa da camelCase in snakeCase
     *
     * @param string $str
     * @return string
     */
    public static function toSnakeCase(string $str): string
    {
        $re    = "/([A-Z]+)/";
        $subst = "_$1";
        return strtolower(preg_replace($re, $subst, $str));
    }

    /**
     * converte una stringa in formato +\-field in un array
     * con chiave "field" e value: "+" => ASC, "-" => DESC
     *
     * @param string $str
     * @return array
     */
    public static function strToQuerySort(string $str): array
    {
        $order = [
            '+' => 'ASC',
            '-' => 'DESC',
        ];

        $symbol = substr($str, 0, 1);
        $field  = substr($str, 1);

        return [self::toSnakeCase($field) => $order[$symbol]];
    }

    public static function arrayToQuerySort(array $data): array
    {
        $list = [];
        foreach ($data as $str) {
            $list += self::strToQuerySort($str);
        }

        return $list;
    }

}

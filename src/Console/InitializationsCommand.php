<?php

namespace App\Console;

use App\Domain\Address\Services\AddressCreateService;
use App\Domain\User\Services\UserCreateService;
use App\Support\Date;
use App\Support\Password;
use Slim\App;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

final class InitializationsCommand extends Command
{
    const NAME = 'app:init';

    /**
     * @var App
     */
    private $app;
    private $user;
    private $address;

    public function __construct(App $app, UserCreateService $user, AddressCreateService $address)
    {
        $this->app     = $app;
        $this->user    = $user;
        $this->address = $address;
        parent::__construct();
    }

    protected function configure(): void
    {
        parent::configure();

        $this->setName(self::NAME);
        $this->setDescription('Initialize the application');
        $this->setHelp('');

    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $io = new SymfonyStyle($input, $output);

        $io->title('Initialization');

        $addresses = [
            [
                'street'      => 'Corso Buenos Aires, 44',
                'city'        => 'Milano',
                'postalCode'  => '20124',
                'countryCode' => 'IT',
                'lat'         => '45.48124377',
                'lng'         => '9.21126552',
            ],
            [
                'street'      => '60 Rue de Ponthieu',
                'city'        => 'Paris',
                'postalCode'  => '75008',
                'countryCode' => 'FR',
                'lat'         => '48.8722488',
                'lng'         => '2.305658',
            ],
        ];

        for ($i = 0; $i < 2; $i++) {

            $password = Password::make(6, Password::LOWER_CASE | Password::UPPER_CASE | Password::NUMBER | Password::SYMBOL);
            $email    = "mail_test_$i@mail.com";

            $user_id = $this->user->create([
                'password'      => $password,
                'email'         => $email,
                'given_name'    => "given_name_test_$i",
                'family_name'   => "family_name_test_$i",
                'date_of_birth' => Date::randomDate('1980-01-01', '2022-08-01'),
                'enabled'       => true,
            ]);

            $this->address->create([
                'user_id'      => $user_id,
                'street'       => $addresses[$i]['street'],
                'city'         => $addresses[$i]['city'],
                'postal_code'  => $addresses[$i]['postalCode'],
                'country_code' => $addresses[$i]['countryCode'],
                'lat'          => $addresses[$i]['lat'],
                'lng'          => $addresses[$i]['lng'],
            ]);

            $row = [
                'email'    => $email,
                'password' => $password,
            ];

            $io->info('created user ' . $i);
            $io->table(['email', 'password'], [$row]);

        }

        $io->success('successful');

        return 0;
    }
}

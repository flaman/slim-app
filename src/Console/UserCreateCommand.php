<?php

namespace App\Console;

use App\Domain\User\Services\UserCreateService;
use Slim\App;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

final class UserCreateCommand extends Command
{
    const NAME = 'user:create';

    /**
     * @var App
     */
    private $app;
    private $user;

    public function __construct(App $app, UserCreateService $user)
    {
        $this->app  = $app;
        $this->user = $user;
        parent::__construct();
    }

    protected function configure(): void
    {
        parent::configure();

        $this->setName(self::NAME);
        $this->setDescription('Create a user');
        $this->setHelp('');
        $this->addArgument('givenName', InputArgument::REQUIRED);
        $this->addArgument('familyName', InputArgument::REQUIRED);
        $this->addArgument('email', InputArgument::REQUIRED);
        $this->addArgument('password', InputArgument::REQUIRED);

    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $io = new SymfonyStyle($input, $output);

        $io->title('New user');

        $this->user->create([
            'password'    => $input->getArgument('password'),
            'email'       => $input->getArgument('email'),
            'given_name'  => $input->getArgument('givenName'),
            'family_name' => $input->getArgument('familyName'),
            'enabled'     => true,
        ]);

        $io->success('successful');

        return 0;
    }
}

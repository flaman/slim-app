<?php

namespace App\Console;

use Slim\App;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

final class RouteListCommand extends Command
{

    const NAME = 'app:route_list';

    /**
     * @var App
     */
    private $app;

    public function __construct(App $app)
    {
        $this->app = $app;
        parent::__construct();
    }

    protected function configure(): void
    {
        parent::configure();

        $this->setName(self::NAME);
        $this->setDescription('List of routes');
        $this->setHelp('List of routes help');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        (require __DIR__ . '/../../config/routes.php')($this->app);

        $io = new SymfonyStyle($input, $output);

        $io->title('Routes');

        $rows = [];

        $routes = $this->app->getRouteCollector()->getRoutes();

        if (!$routes) {
            $io->text('Routes list is empty');
            return 0;
        }
        foreach ($routes as $route) {
            $rows[] = [
                'path'    => $route->getPattern(),
                'methods' => implode(', ', $route->getMethods()),
                'name'    => $route->getName(),
                'handler' => $route->getCallable(),
            ];
        }
        $io->table(
            ['Route', 'Methods', 'Name', 'Handler'],
            $rows
        );
        return 0;
    }
}

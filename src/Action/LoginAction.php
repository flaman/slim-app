<?php

namespace App\Action;

use App\Domain\User\Services\UserAuthService;
use App\Renderer\JsonRenderer;
use App\Support\Token;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class LoginAction
{
    private $renderer;
    private $user;
    private $token;

    public function __construct(
        ContainerInterface $container,
        JsonRenderer $renderer,
        UserAuthService $user,
        Token $token
    ) {
        $this->renderer = $renderer;
        $this->user     = $user;
        $this->token    = $token;

        $this->token->setJwtSecret(
            $container->get('settings')['JWT_SECRET']
        );
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $parsedBody = $request->getParsedBody();

        $user = $this->user->login($parsedBody['email'], $parsedBody['password']);

        if (!$user) {
            return $this->renderer
                ->json($response, ["title" => "email or password wrong", "status" => 401])
                ->withStatus(401);
        }

        $jwt = $this->token->get($user);

        return $this->renderer
            ->json($response, $jwt)
            ->withStatus(200);
    }
}

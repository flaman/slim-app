<?php

namespace App\Action\User;

use App\Domain\User\Services\UsersService;
use App\Renderer\JsonRenderer;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class UsersAction
{
    private $renderer;
    private $users;

    public function __construct(
        JsonRenderer $renderer,
        UsersService $users
    ) {
        $this->renderer = $renderer;

        $this->users = $users;

    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {

        $params = $request->getQueryParams();

        $where = [];

        if (isset($params['countryCode'])) {
            $where['country_code'] = $params['countryCode'];
        }

        if (isset($params['email'])) {
            $where['email'] = $params['email'];
        }

        $sort     = $params['sort'] ?? [];
        $page     = $params['page'] ?? 1;
        $per_page = $params['perPage'] ?? 1;

        $users = $this->users->getAll(
            $where,
            $sort,
            $page,
            $per_page
        );

        return $this->renderer
            ->json($response, ['totalItems' => count($users), 'items' => $users])
            ->withStatus(200);
    }
}

<?php

namespace App\Action\User;

use App\Domain\User\Services\UserCreateService;
use App\Domain\User\Services\UsersService;
use App\Renderer\JsonRenderer;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class UserCreateAction
{
    private $renderer;
    private $users;
    private $user_create;
    private $address_create;

    public function __construct(
        JsonRenderer $renderer,
        UserCreateService $user_create,
        UsersService $users,
    ) {
        $this->renderer    = $renderer;
        $this->users       = $users;
        $this->user_create = $user_create;

    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {

        $parsedBody = $request->getParsedBody();

        $user_id = $this->user_create->create([
            'password'      => $parsedBody['password'],
            'email'         => $parsedBody['email'],
            'given_name'    => $parsedBody['givenName'],
            'family_name'   => $parsedBody['familyName'],
            'date_of_birth' => $parsedBody['dateOfBirth'],
            'enabled'       => true,
        ]);

        $user = $this->users->getById($user_id);

        return $this->renderer
            ->json($response, ['data' => $user])
            ->withStatus(200);
    }
}

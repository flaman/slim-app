<?php

namespace App\Action\User;

use App\Domain\User\Services\UsersService;
use App\Renderer\JsonRenderer;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class UserFindByEmailAction
{
    private $renderer;
    private $users;

    public function __construct(
        JsonRenderer $renderer,
        UsersService $users
    ) {
        $this->renderer = $renderer;

        $this->users = $users;

    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {

        $user = $this->users->getByEmail($args['email']);

        return $this->renderer
            ->json($response, ['data' => $user])
            ->withStatus(200);
    }
}

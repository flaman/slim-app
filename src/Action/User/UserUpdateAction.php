<?php

namespace App\Action\User;

use App\Domain\User\Services\UsersService;
use App\Domain\User\Services\UserUpdateService;
use App\Renderer\JsonRenderer;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class UserUpdateAction
{
    private $renderer;
    private $user_update;
    private $users;

    public function __construct(
        JsonRenderer $renderer,
        UserUpdateService $user_update,
        UsersService $users
    ) {
        $this->renderer    = $renderer;
        $this->user_update = $user_update;
        $this->users       = $users;

    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {

        $parsedBody = $request->getParsedBody();

        $data = [
            'password'      => $parsedBody['password'],
            'email'         => $parsedBody['email'],
            'given_name'    => $parsedBody['givenName'],
            'family_name'   => $parsedBody['familyName'],
            'date_of_birth' => $parsedBody['dateOfBirth'],
        ];

        $this->user_update->updateByEmail($args['email'], $data);

        $user = $this->users->getByEmail($args['email']);

        return $this->renderer
            ->json($response, ['data' => $user])
            ->withStatus(200);
    }
}

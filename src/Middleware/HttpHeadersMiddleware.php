<?php

namespace App\Middleware;

use App\Renderer\JsonRenderer;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class HttpHeadersMiddleware implements MiddlewareInterface
{

    private ResponseFactoryInterface $responseFactory;
    private JsonRenderer $renderer;

    public function __construct(ResponseFactoryInterface $responseFactory, JsonRenderer $jsonRenderer)
    {
        $this->responseFactory = $responseFactory;
        $this->renderer        = $jsonRenderer;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $header_content_type = $request->getHeaderLine("Content-Type");
        $header_accept       = $request->getHeaderLine("Accept");

        if (
            $header_accept == 'application/json' &&
            $header_content_type == 'application/json'
        ) {

            return $handler->handle($request);
        }

        $response = $this->responseFactory->createResponse();
        return $this->renderer
            ->json($response, ["title" => "Not Acceptable", "status" => 406], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES)
            ->withStatus(406);
    }
}

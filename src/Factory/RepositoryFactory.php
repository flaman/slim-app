<?php

namespace App\Factory;

use Cake\Database\Connection;
use Cake\Database\Query;

/**
 * Repository.
 */
final class RepositoryFactory
{
    /**
     * connection
     *
     * @var Connection
     */
    private $connection;

    /**
     * Il costruttore
     *
     * @param Connection $connection The database connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * crea un oggetto query
     *
     * @return Query The query
     */
    public function query(): Query
    {
        return $this->connection->newQuery();
    }

    /**
     * crea una select
     *
     * @param string $table
     * @param array $where
     * @param array $fields
     * @return Query
     */
    public function select(string $table, array $where, $fields = []): Query
    {
        return $this->query()
            ->select($fields)
            ->from($table)
            ->where($where);
    }

    /**
     * crea un update
     *
     * @param string $table
     * @param array $data
     * @param array $where
     * @return Query
     */
    public function update(string $table, array $data, array $where): Query
    {
        return $this->query()
            ->update($table)
            ->set($data)
            ->where($where);
    }

    /**
     * crea una insert
     *
     * @param string $table
     * @param array $data
     * @return Query
     */
    public function insert(string $table, array $data): Query
    {
        return $this->query()
            ->insert(array_keys($data))
            ->into($table)
            ->values($data);
    }

    /**
     * crea una delete
     *
     * @param string $table
     * @param array $where
     * @return Query
     */
    public function delete(string $table, array $where): Query
    {
        return $this->query()
            ->delete($table)
            ->where($where);
    }
}

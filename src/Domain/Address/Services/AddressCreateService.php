<?php

namespace App\Domain\Address\Services;

use App\Domain\Address\Repository\AddressRepository;
use DomainException;

class AddressCreateService
{
    private $address;

    public function __construct(AddressRepository $address)
    {
        $this->address = $address;
    }

    public function create(array $data): ?int
    {
        if (empty($data['user_id'])) {
            throw new DomainException("User id empty");
        }

        return $this->address->insert($data);
    }
}

<?php

namespace App\Domain\Address\Services;

use App\Domain\Address\Repository\AddressRepository;

class AddressUpdateService
{
    private $address;

    public function __construct(AddressRepository $address)
    {
        $this->address = $address;
    }

    public function updateByUserId(int $user_id, array $data): ?int
    {

        if (!$this->address->get(['id' => $user_id], ['id'])) {

            $data['user_id'] = $user_id;

            return $this->address->insert($data);
        }

        return $this->address->update($data, ['user_id' => $user_id]);
    }
}

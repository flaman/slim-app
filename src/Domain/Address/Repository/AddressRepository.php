<?php

namespace App\Domain\Address\Repository;

use App\Factory\RepositoryFactory;

class AddressRepository
{

    const TABLE = 'addresses';

    private $query;

    public function __construct(RepositoryFactory $query)
    {
        $this->query = $query;

    }

    public function get(array $where, array $fields = ['*']): bool | array
    {
        return $this->query
            ->select(self::TABLE, $where, $fields)
            ->execute()
            ->fetch('assoc');
    }

    public function insert(array $data): int
    {
        return (int) $this->query
            ->insert(self::TABLE, $data)
            ->execute()
            ->lastInsertId();
    }

    public function update(array $data, array $where): int
    {
        return (int) $this->query
            ->update(self::TABLE, $data, $where)
            ->execute()
            ->errorCode();
    }

}

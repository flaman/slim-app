<?php

namespace App\Domain\User\Repository;

use App\Factory\RepositoryFactory;

class UserRepository
{

    const TABLE = 'users';

    private $query;

    public function __construct(RepositoryFactory $query)
    {
        $this->query = $query;

    }

    public function existEmail(string $email): bool
    {
        return $this->query
            ->select(self::TABLE, ['email' => $email], ['id'])
            ->execute()
            ->rowCount() > 0;
    }

    public function getAll(array $fields = ['*'], array $where = [], array $sort = [], int $page = 0, $per_page = 1)
    {
        $query_builder = $this->query->query();

        $query_builder->select($fields);
        $query_builder->from(self::TABLE);
        $query_builder->leftJoin('addresses', 'addresses.user_id = users.id');

        foreach ($where as $field => $value) {
            $query_builder->whereInList($field, $value);
        }

        if (!empty($sort)) {
            $query_builder->order($sort);
        }

        $query_builder->offset($page * $per_page);

        $query_builder->limit($per_page);

        return $query_builder->execute()->fetchAll('assoc');

    }

    public function get(array $where, array $fields = ['*']): bool | array
    {
        return $this->query
            ->select(self::TABLE, $where, $fields)
            ->leftJoin('addresses', 'addresses.user_id = users.id')
            ->execute()
            ->fetch('assoc');
    }

    public function getUserByMail(string $email, array $fields = ['*']): bool | array
    {
        return $this->get(['email' => $email], $fields);
    }

    public function insert(array $data): int
    {
        return (int) $this->query
            ->insert(self::TABLE, $data)
            ->execute()
            ->lastInsertId();
    }

    public function update(array $data, array $where): int
    {
        return (int) $this->query
            ->update(self::TABLE, $data, $where)
            ->execute()
            ->errorCode();
    }

}

<?php

namespace App\Domain\User\Services;

use App\Domain\User\Repository\UserRepository;
use App\Support\Strings;

class UsersService
{
    private $user;

    private $fields = [
        "users.id",
        "users.username",
        "users.password",
        "users.email",
        "users.given_name",
        "users.family_name",
        "users.date_of_birth",
        "users.created_at",
        "users.enabled",
        "addresses.user_id",
        "addresses.street",
        "addresses.city",
        "addresses.postal_code",
        "addresses.country_code",
        "addresses.lat",
        "addresses.lng",
    ];

    public function setFields(array $fields)
    {
        $this->fields = $fields;
    }

    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }

    private function format(array $user)
    {
        return [
            'givenName'   => $user['given_name'],
            'familyName'  => $user['family_name'],
            'email'       => $user['email'],
            'dateOfBirth' => $user['date_of_birth'],
            'createdAt'   => $user['created_at'],
            'address'     => [
                'street'      => $user['street'],
                'city'        => $user['city'],
                'postalCode'  => $user['postal_code'],
                'countryCode' => $user['country_code'],
                'coordinates' => [
                    'lat' => $user['lat'],
                    'lng' => $user['lng'],
                ],
            ],
        ];

    }

    public function getAll(array $where_in = [], array $sort = [], int $page = 0, int $per_page = 1)
    {

        // il primo blocco di dati parte da page - 1
        if ($page > 0) {
            $page -= 1;
        }

        if (!empty($sort)) {
            $sort = Strings::arrayToQuerySort($sort);
        }

        $users = $this->user->getAll(
            $this->fields,
            $where_in,
            $sort,
            $page,
            $per_page
        );

        $data = [];
        foreach ($users as $user) {
            $data[] = $this->format($user);
        }

        return $data;
    }

    public function getById(int $id)
    {
        $user = $this->user->get(['users.id' => $id], $this->fields);

        return $this->format($user);
    }

    public function getByEmail(string $email)
    {
        $user = $this->user->get(['users.email' => $email], $this->fields);

        return $this->format($user);
    }

    public function exist(string $email)
    {
        return $this->user->existEmail($email);
    }

}

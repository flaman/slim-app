<?php

namespace App\Domain\User\Services;

use App\Domain\User\Repository\UserRepository;

class UserAuthService
{
    private $user;

    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }

    public function login(string $email, string $password): ?array
    {
        $user = $this->user->getUserByMail($email);

        if (!$user) {
            return null;
        }

        if (!password_verify($password, $user['password'])) {
            return null;
        }

        return $user;
    }

    public function logout()
    {
        // TODO: da implementare
    }
}

<?php

namespace App\Domain\User\Services;

use App\Domain\User\Repository\UserRepository;
use App\Support\Password;
use DomainException;

class UserCreateService
{
    private $user;

    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }

    public function create(array $data): ?int
    {
        if ($this->user->existEmail($data['email'])) {
            throw new DomainException("User email exist: {$data['email']}");
        }

        $flag = Password::NUMBER | Password::LOWER_CASE | Password::UPPER_CASE | Password::SYMBOL;

        if (!Password::validate($data['password'], 6, $flag)) {
            throw new DomainException("User password wrong 1: {$data['password']}");
        }

        if (strtok($data['email'], '@') == $data['password']){
            throw new DomainException("User password wrong 2: {$data['password']}");
        }

        $data['password'] = password_hash($data['password'], PASSWORD_BCRYPT, ['cost' => 10]);

        return $this->user->insert($data);
    }
}

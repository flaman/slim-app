<?php

namespace App\Domain\User\Services;

use App\Domain\User\Repository\UserRepository;
use DomainException;

class UserUpdateService
{
    private $user;

    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }

    public function updateByEmail(string $email, array $data): ?int
    {
        if (!$this->user->existEmail($email)) {
            throw new DomainException("User email not exists: {$email}");
        }

        if ($email != $data['email'] && $this->user->existEmail($data['email'])) {
            throw new DomainException("User email exists for another user: {$data['email']}");
        }

        return $this->user->update($data, ['email' => $email]);
    }
}

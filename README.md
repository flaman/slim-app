# Setup iniziale

```
docker-compose up -d --build
docker-compose exec webserver composer install
docker-compose exec webserver php vendor/bin/phoenix migrate
docker-compose exec webserver php bin/console user:create given_name_test family_name_test test@mail.com Test_User_1
```

# Creazione 2 utenti fittizi

```
docker-compose exec webserver php bin/console app:init
```

# Tutte le rotte

```
docker-compose exec webserver php bin/console app:route_list
```